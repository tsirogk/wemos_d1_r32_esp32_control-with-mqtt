## ESP32 Series projects

**Control a LED or a relay with mqtt and esp32**

I use a refurbished pc in a static ip for production and for developement I use a raspberry pi with dynamic dns. Both have mosquitto and linux. It works.

I can install whatever software I want. I have installled influx db, telegraf, grafana and mqtt, so I have a complete iot solution.


---

What you need.

1. ESP32 module. I used a wemos d1 r32
2. Relay module
3. mqtt broker public or private
